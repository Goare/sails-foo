/**
 * InvitationCodeController
 *
 * @description :: Server-side logic for managing Invitationcodes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	generate: function(req, res)
  {
    var code = UserService.generateRandomPassword();
    InvitationCode
      .create({
      code: code,
      valid: true
    })
      .then(function(code){
      return res.json(code);
    })
      .catch(function(err){
        return sails.log.error('generate invitation code error: ', err);
      })
  }
};

