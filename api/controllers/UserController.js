/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	login  : function(req, res) {
    var email = req.body.email;
    var password = req.body.password;

    UserService.auth(email, password)
      .then(function(data){

        sails.log.debug('data', data);

        if (data.status)
        {
          req.session.userEmail = data.userEmail;
          req.session.authenticated = true;

					// root user
          if (email === 'mygoare@qq.com')
            req.session.isRoot = true;
        }

        return res.json(data)
      })
      .catch(function(err){
        return sails.log.error('User auth err: ', err);
      })
	},
	signup : function(req, res) {
    var email = req.body.email;
    var invitationCode = req.body.code;

    var checkCode = UserService.checkInvitationCode(invitationCode);
		var deleteCode = UserService.deleteInvitationCode(invitationCode);
		var checkEmail = UserService.checkEmail(email);

    checkCode.then(function(data){
      if (data.status)
      {
				checkEmail
				.then(function(data){
					sails.log.debug(data);

					if (data.status)
					{
						return res.json(data); // email user exists
					}
					else
					{
						deleteCode
						.then(function(){
							UserService.register(email)
							.then(function(user){
								var userEmail = user.email;
								var userPass  = user.password;

								EmailService.sendWelcomeEmail({
										to: userEmail,
										subject: 'Welcome to ss-vv service',
										text: '欢迎您注册ss-vv, 这是您的密码: ' + userPass
									})
									.then(function(data){
										return res.json(data)
									});
							})
						})
					}
				})
      }
      else
      {
        return res.json(data)
      }
    })


	},
	resetPassword: function(req, res) {
    var email = req.body.email;

    UserService.checkEmail(email)
      .then(function(data){
        if (data.status)
        {
          // user exists

          return EmailService.sendEmail({
            to: email,
            subject: 'Your Reset Password Request Email',
            text: 'Your are asking to reset your ss-vv password, here is the link: xxxxxlinkxxxxx'
          })
          .then(function(data){
            return res.json(data)
          })
        }
        else
        {
          return res.json(data)
        }
      })
	},
	changePassword: function(req, res){
		var oldPw = req.body.oldPassword;
		var newPw = req.body.newPassword;
		var email = req.session.userEmail;

		UserService.auth(email, oldPw)
			.then(function(data){
				if (data.status)
				{
					UserService.updatePassword(email, newPw)
					.then(function(data){
						return res.json(data)
					})
				}
				else
				{
					return res.json(data);
				}
			})
	},
	logout : function(req, res) {
    req.session.destroy(function(err){
      if (err)
        sails.log.error('logout error: ', err);

      return res.redirect('/');
    })
	},



  dashboard: function(req, res)
  {
    res.send('you see me because you are login in.')
  }
};
