/**
 * FileController
 *
 * @description :: Server-side logic for managing files
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Promise = require('bluebird');

const maxBytes = 1024 * 1024 * 30;

module.exports = {
	upload: function(req, res) {
    req.file('files').upload(
      {
        maxBytes: maxBytes
      },
      function(err, files) {
      if (err)
        return res.serverError(err);

      sails.log.info('files done: ', files);

      Promise.all(
        files.map(function(file, index){
          return uploadToQiniu(file)
        })
      )
      .then(function(messages){
        res.json(messages);
      })
    });

    function uploadToQiniu(file) {
      return QiniuService.uploadPublic(file.fd, file.filename)
        .then(function(file){
          sails.log.debug(file.hash, file.key, file.persistentId);
          return file;
        }, function(err){
          sails.log.error('uploadToQiniu error: ', err);
          return err;
        })
    }
  }
};

