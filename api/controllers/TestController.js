module.exports = {
    index: function(req, res)
    {
        Student.create({
            firstName: 'goare',
            lastName : 'mao',
            career   : 'programmer',
            email    : 'mygoare@gmail.com'
        }).then(function(student){
            student.subscriptionLists.add({
                name: 'test',
                description: 'test-test description'
            });

            student.save(function(err){
                if (err) {
                    return res.send(err)
                }
                else
                {
                    Student.create({
                        firstName: 'haha',
                        lastName : 'go',
                        email    : '123@abc.com'
                    }).then(function(){
                        return res.send('hello');
                    })
                }
            })
        })
    },
    add: function(req, res)
    {
        Student.findOne(4).then(function(student){
            student.subscriptionLists.add(1);

            student.save(function(err){
                if (err){
                    return res.send(err)
                }
                else
                {
                    return res.send('success')
                }
            })
        })
    },
    find: function(req, res)
    {
        Student.findOne(4)
            .populate('subscriptionLists')
            .then(function(student){
                return res.send(student)
            })
            .catch(function(err){
                return res.send(err)
            })
    }
}
