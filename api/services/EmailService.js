var Promise = require('bluebird');
var request = Promise.promisify(require('request'));

var mailgunApiKey = 'key-92433b2f3ca07cf08e5f5d764b89a26f';
var mailgunApiBaseUrl = 'https://api.mailgun.net/v3/sandbox0f93fe2fc783427e8ce560d9df497d35.mailgun.org';

var emailSender = 'ss-vv <no-reply@ss-vv.com>';

module.exports = {
  sendEmail: function(opts)
  {
    var url = 'https://api:'+mailgunApiKey+'@api.mailgun.net/v3/sandbox0f93fe2fc783427e8ce560d9df497d35.mailgun.org/messages';
    return request({
      url: url,
      method: 'POST',
      form: {
        from: opts.from || emailSender,
        to: opts.to,
        subject: opts.subject,
        text: opts.text
      }
    })
      .then(function(res){
        sails.log.debug(res.body);

        return HelperService.return(1, JSON.parse(res.body))
      })
      .catch(function(err){

      })
  },
  sendWelcomeEmail: function(opts){
    return this.sendEmail({
      from: opts.from || emailSender,
      to: opts.to,
      subject: opts.subject,
      text: opts.text
    })
  }
};
