var Promise = require('bluebird');
var bcrypt = Promise.promisifyAll(require('bcrypt'));

var info = {
  loginSuccess  : 'login success',
  loginFail     : 'email or password wrong',
  emailExist    : 'email exist',
  emailNotExist : 'email does not exist'
};

module.exports = {
  generateRandomPassword: function()
  {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var length = 8;
    var pass = "";

    for(var i = 0; i<length; i++)
    {
      var n = Math.floor(Math.random()*chars.length);
      pass += chars.charAt(n);
    }
    return pass;
  },
  register: function(email)
  {
    var password = this.generateRandomPassword();
    return User.create({
      email: email,
      name: email.split('@')[0],
      password: password
    })
      .then(function(user){
        return {email: user.email, password: password}
      })
  },
  updatePassword: function(email, password)
  {
    return User.update(
      {email: email},
      {password: password}
    )
    .then(function(){
      return {status: 1, data: 'User password updated'}
    })
  },
  checkInvitationCode: function(code)
  {
    return InvitationCode.findOne({
      code: code
    })
    .then(function(record){
      sails.log.debug('checkInvitationCode', record);
      if (!record)
        return {status: 0, info: 'invitationCode does not exist'}
      
      if (record && record.valid) {
        return {status: 1, info: 'invitationCode valid'}
      }
      else
      {
        return {status: 0, info: 'invitationCode invalid'}
      }
    })
  },
  deleteInvitationCode: function(code)
  {
    return InvitationCode.update(
      {code: code}, 
      {valid: false}
   );
  },
  checkEmail: function(email)
  {
    return User.findOne({
      email: email
    })
      .then(function(user){
        return user ? HelperService.return(1, info.emailExist) : HelperService.return(0, info.emailNotExist);
      });
  },
  auth: function(email, password)
  {
      return User.findOne({
          email: email
        })
        .then(function(user){
          if (!user)
            return {status: 0, data: info.loginFail};
          else
          {
            return bcrypt.compareAsync(password, user.password)
                    .then(function(isMatch){
                      sails.log.debug(password, user.password, isMatch);

                      return isMatch ? {status: 1, data: info.loginSuccess, userEmail: user.email} : {status: 0, data: info.loginFail}
                    })
          }
        })
        .catch(function(err) {
          sails.log.error(err);
          return {status: 0, data: err}
        })
  }
};
