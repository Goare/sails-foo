module.exports = {
  return: function(status, data)
  {
    return {
      status : status,
      data   : data
    }
  }
};
