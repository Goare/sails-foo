var Promise = require('bluebird');
var qiniu = require("qiniu");
var unlinkAsync = Promise.promisify(require('fs').unlink);

//需要填写你的 Access Key 和 Secret Key
qiniu.conf.ACCESS_KEY = 'GuQFPqBFFH2WQ4jhL5x6dMM-u6ToqBWlw2TwDQPF';
qiniu.conf.SECRET_KEY = '5LEbeShe_0LHyIRabvBjAWN7IPNS9tUq4ydcIrt-';

var qiniuBucket = {
  public : 'test-public',
  private: 'test-private'
};


module.exports = {
  uploadPublic: function(localFilePath, qiniuKeyName) {
    //要上传的空间
    var bucket = qiniuBucket.public;

    //上传到七牛后保存的文件名
    var key = Date.now() + '-' + qiniuKeyName;

    //构建上传策略函数
    function uptoken(bucket, key) {
      var putPolicy = new qiniu.rs.PutPolicy(bucket+":"+key);
      return putPolicy.token();
    }

    //生成上传 Token
    var token = uptoken(bucket, key);

    //要上传文件的本地路径
    var filePath = localFilePath;

    //构造上传函数
    return new Promise(function upload(resolve, reject){
      var extra = new qiniu.io.PutExtra();
      qiniu.io.putFile(token, key, filePath, extra, function(err, file) {
        if(!err) {
          unlinkAsync(localFilePath)
            .then(function(){
              // 上传成功， 处理返回值
              resolve(file);
            }, function (err) {
              reject(err)
            })
        } else {
          // 上传失败， 处理返回代码
          reject(err);
        }
      });
    });
  }
};
