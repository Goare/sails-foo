/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');
module.exports = {

  attributes: {
      email: {
          type: 'email',
          unique: true,
          required: true
      },
      name: {
          type: 'string',
          required: true
      },
      password: {
          type: 'string',
          required: true,
          minLength: 6
      }
  },

  beforeCreate: function(data, cb) {
      var password = data.password;
      bcrypt.hash(password, 10, function(err, hash){
         if (err)  return cb(err);

         data.password = hash;
         cb();
      });
  },
  beforeUpdate: function(data, cb) {
    sails.log.debug('beforeUpdate data: ', data);
    var password = data.password;
    if (password)
    {
      bcrypt.hash(password, 10, function(err, hash){
         if (err)  return cb(err);

         data.password = hash;
         cb();
      });
    }
    else
    {
      cb();
    }
  }
};
